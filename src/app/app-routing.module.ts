import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ResgisterUserComponent } from './components/resgister-user/resgister-user.component';
import { CuentasComponent } from './components/cuentas/cuentas.component';
import { TransferenciaComponent } from './components/transferencia/transferencia.component';
import { CuentaComponent } from './components/cuenta/cuenta.component';
import { AuthGuardService } from './services/auth-guard.service';


const routes: Routes = [
  {path: '', component: CuentasComponent, canActivate: [AuthGuardService]},
  {path: 'registro', component: ResgisterUserComponent},
  {path: 'login', component: LoginComponent},
  {path: 'transferencia', component: TransferenciaComponent, canActivate: [AuthGuardService]},
  {path: 'cuenta', component: CuentaComponent, canActivate: [AuthGuardService]},
  { path: '**', redirectTo: 'login', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
