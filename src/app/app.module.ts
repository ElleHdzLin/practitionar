import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './shared/header/header.component';
import { ResgisterUserComponent } from './components/resgister-user/resgister-user.component';
import { CuentasComponent } from './components/cuentas/cuentas.component';
import { CuentaComponent } from './components/cuenta/cuenta.component';
import { TransferenciaComponent } from './components/transferencia/transferencia.component';
import { LoginService } from './services/login.service';
import { ShareDataService } from './services/share-data.service';
import { DebitComponent } from './pages/debit/debit.component';
import { CreditComponent } from './pages/credit/credit.component';
import { LoanComponent } from './pages/loan/loan.component';
import { AuthGuardService } from './services/auth-guard.service';
import { AccountsService } from './services/accounts.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    ResgisterUserComponent,
    CuentasComponent,
    CuentaComponent,
    TransferenciaComponent,
    DebitComponent,
    CreditComponent,
    LoanComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    LoginService,
    ShareDataService,
    AuthGuardService,
    AccountsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
