import { Injectable } from '@angular/core';
import {loginI} from '../models/login.model';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient, private router: Router) { }

  Headers: HttpHeaders;
  url = 'http://localhost:3000'
  public user = {};

  login(user: loginI){
    this.Headers = new HttpHeaders().set('Content-type', 'application/json');
    return this.http.post(`${this.url}/login`, user, {headers: this.Headers});
  }

  registerUser(user: any){
    this.Headers = new HttpHeaders().set('Content-type', 'application/json');
    return this.http.post(`${this.url}/usuarioRegistrado`, user, {headers: this.Headers});
  }

  getSesion(){
    this.user = JSON.parse(localStorage.getItem('user'));
    return localStorage.getItem('user');
  }

  setSession(user){
    localStorage.setItem('user', JSON.stringify({...user.data, token: user.token}));
    this.user = user;
    this.router.navigate(['/']);
  }

  logOut(){
    localStorage.clear();
    this.router.navigate(['/login']);
  }
}
