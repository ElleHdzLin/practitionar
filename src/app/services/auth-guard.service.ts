import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private _login: LoginService, private router: Router) { }

  canActivate(): boolean {
    if(this._login.getSesion()){
      return true;
    }else{
      this.router.navigate(['/login']);
      return false;
    }
  }
}
