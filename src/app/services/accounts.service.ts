import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AccountsService {

  constructor(private http: HttpClient, private router: Router) { }

  private Headers: HttpHeaders = new HttpHeaders();
  private url = 'http://localhost:3000';
  private user = JSON.parse(localStorage.getItem('user'));

  getAccounts(){
    this.Headers.set('Content-type', 'application/json');    
    return this.http.post(`${this.url}/cuenta?token=${this.user.token}`, {id_usuario: this.user._id}, {headers: this.Headers});
  }

  getMovements(id){
    this.Headers.set('Content-type', 'application/json'); 
    return this.http.post(`${this.url}/movimiento/?token=${this.user.token}`, {id_cuenta: id}, {headers: this.Headers});
  }

  newTranfer(data){
    this.Headers.set('Content-type', 'application/json'); 
    return this.http.post(`${this.url}/movimiento/transferencia/?token=${this.user.token}`, data, {headers: this.Headers});
  }
}
