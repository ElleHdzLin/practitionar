import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from "rxjs/index";

@Injectable({
  providedIn: 'root'
})
export class ShareDataService {

  constructor() { }
  private title = "Inicia Sesion"
  private accounts = [];
  private movement = {};
  private modificate: BehaviorSubject<string> = new BehaviorSubject(this.title);
  private modiAccounts: BehaviorSubject<any> = new BehaviorSubject(this.accounts);
  private modiMovement: BehaviorSubject<any> = new BehaviorSubject(this.movement);

  getTitle(): Observable<string> {
    return this.modificate.asObservable();
  }

  setTitle(title) {
    this.title = title;
    this.modificate.next(title);
  }

  getAccounts(): Observable<any> {
    return this.modiAccounts.asObservable();
  }

  setAccounts(accounts) {
    this.accounts = accounts;
    this.modiAccounts.next(accounts);
  }

  setMovement(movement){
    this.movement = movement;
    this.modiMovement.next(movement);
  }

  getMovement(){
    return this.modiMovement.asObservable();
  }
}
