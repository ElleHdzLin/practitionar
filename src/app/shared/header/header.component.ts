import { Component, OnInit } from '@angular/core';
import { ShareDataService } from 'src/app/services/share-data.service';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public title: string;

  constructor(private _shared: ShareDataService, private _login: LoginService) { }

  ngOnInit() {
    this._shared.getTitle().subscribe(res =>{
      this.title = res;
    });
  }

  logOut(){
    this._login.logOut();
  }

}
