import { Component, OnInit } from '@angular/core';
import { ShareDataService } from 'src/app/services/share-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-credit',
  templateUrl: './credit.component.html',
  styleUrls: ['../debit/debit.component.scss']
})
export class CreditComponent implements OnInit {
  public cuentas = [];
  public path = ""
  constructor(private _shared: ShareDataService, private router: Router) { }

  ngOnInit() {
    this.path = this.router.url;
    this._shared.getAccounts().subscribe(res =>{
      this.cuentas = [];
      res.forEach(cuenta =>{
        if(cuenta.tipo_cuenta === "credito"){
          this.cuentas.push(cuenta)
        }
      });
    });
  }

  returnCuenta(cuenta){
    var cu = [];
    cu.push(cuenta)
    this._shared.setAccounts(cu);
    this.router.navigate(['/cuenta']);
  }

}
