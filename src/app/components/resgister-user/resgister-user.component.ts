import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/login.service';
import { FormControl, FormGroup } from '@angular/forms';
import { ShareDataService } from 'src/app/services/share-data.service';
import { Router } from '@angular/router';

declare var swal:any;

@Component({
  selector: 'app-resgister-user',
  templateUrl: './resgister-user.component.html',
  styleUrls: ['./resgister-user.component.scss']
})
export class ResgisterUserComponent implements OnInit {

  crateFormGroup() {
    return new FormGroup({
      cuenta: new FormControl(0),
      email: new FormControl(''),
      password: new FormControl('')
    });
  }

  registerForm: FormGroup;

  constructor(private _login: LoginService, private _shared: ShareDataService, private router: Router) { 
    this.registerForm = this.crateFormGroup();
  }

  register(){
    this._login.registerUser(this.registerForm.value).subscribe(res =>{
      console.log('res registro al usuario de manera correcta', res);
      swal("Usuario registrado","","success",{timer:3000});
      this.router.navigate(['/login']);
    }, 
    err=>{
      console.log('tenemos un error al registrar al usuario', err);
      swal("Error al registrar usuario","","error",{timer:2000});
    })
    
  }

  ngOnInit() {
    this._shared.setTitle('Registrate');
  }

}
