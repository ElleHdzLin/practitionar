import { Component, OnInit } from '@angular/core';
import { ShareDataService } from 'src/app/services/share-data.service';
import { AccountsService } from 'src/app/services/accounts.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cuenta',
  templateUrl: './cuenta.component.html',
  styleUrls: ['./cuenta.component.scss']
})
export class CuentaComponent implements OnInit {
  public cuenta = {
    _id: ""
  };

  public movimientos = [];

  constructor(private _shared: ShareDataService, private _account: AccountsService, private router: Router) { }

  ngOnInit() {
    this._shared.setTitle('Tu Cuenta')

    this._shared.getAccounts().subscribe(res => {
      if (res.length == 0) {
        this.router.navigate(['/']);
      } else {
        this.cuenta = res[0];
        this.getMovimientos(this.cuenta._id);
      }
    });
  }

  getMovimientos(id) {
    this._account.getMovements(id).subscribe(res => {
      this.movimientos = [];
      this.movimientos = res['movimiento'];
      console.log(res);
      
      this.formatDate();
    }, err => {
      console.log(err);

    })
  }

  formatDate() {
    this.movimientos.forEach(movimiento => {
      let date = new Date(movimiento.fecha);

      let options = {
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric'
      };
      movimiento.fecha = date.toLocaleDateString('es-MX', options);
    });
  }

  newMovement(type) {
    let movement = {
      type: type,
      account: this.cuenta._id
    }

    this._shared.setMovement(movement);
    this.router.navigate(['/transferencia']);
  }

}
