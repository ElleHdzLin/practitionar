import { Component, OnInit } from '@angular/core';
import { AccountsService } from 'src/app/services/accounts.service';
import { ShareDataService } from 'src/app/services/share-data.service';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

declare var swal: any;

@Component({
  selector: 'app-transferencia',
  templateUrl: './transferencia.component.html',
  styleUrls: ['./transferencia.component.scss']
})
export class TransferenciaComponent implements OnInit {

  crateFormGroup() {
    return new FormGroup({
      nombre: new FormControl(''),
      tipo_movimiento: new FormControl(''),
      monto: new FormControl(''),
      descripcion: new FormControl(''),
      id_cuenta: new FormControl(''),
      cuentaTraspaso: new FormControl('')
    });
  }

  private mov = {
    account: "",
    type: ""
  }

  tranferForm: FormGroup;

  constructor(private _shared: ShareDataService, private _accounts: AccountsService, private router: Router) {
    this.tranferForm = this.crateFormGroup();
  }

  ngOnInit() {
    this._shared.getMovement().subscribe(res => {
      console.log(res);
      if (Object.keys(res).length === 0) {
        this.router.navigate(['/']);
      } else {
        this.mov = res;
        this._shared.setTitle(res.type);
      }
    });
  }

  tranferir() {
    this.tranferForm.value.tipo_movimiento = this.mov.type.toLowerCase();
    this.tranferForm.value.id_cuenta = this.mov.account;
    this.tranferForm.value.nombre = new Date();
    console.log(this.tranferForm.value);

    this._accounts.newTranfer(this.tranferForm.value).subscribe(res => {
      console.log('tranferencia exitosa', res);
      swal("Transferencia exitoza",
        `Transferiste ${this.tranferForm.value.monto} a la cuenta .${this.tranferForm.value.cuentaTraspaso.substr(10, 12)}`,
        "success", { timer: 3000 });
        this.router.navigate(['/']);
    }, err => {
      console.log('error al transferir', err);

    })
  }

}
