import { Component, OnInit } from '@angular/core';
import { ShareDataService } from 'src/app/services/share-data.service';
import { LoginService } from 'src/app/services/login.service';
import { AccountsService } from 'src/app/services/accounts.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cuentas',
  templateUrl: './cuentas.component.html',
  styleUrls: ['./cuentas.component.scss']
})
export class CuentasComponent implements OnInit {

  user: any;

  cuentas = [];

  constructor(private _shared: ShareDataService, private _login: LoginService, private _accounts: AccountsService, private router: Router) { }

  ngOnInit() {
    this.user = this._login.user;
    this._shared.setTitle(`Cuentas de ${this.user.nombre}`);
    this._accounts.getAccounts().subscribe(res =>{
      this._shared.setAccounts(res['cuenta']);
    }, err => {
      console.log('el error', err);
      if(err.status === 401){
        localStorage.clear();
        this.router.navigate(['/login']);
      }
    });
  }

}
