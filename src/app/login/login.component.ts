import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { FormControl, FormGroup } from '@angular/forms';
import { ShareDataService } from '../services/share-data.service';
import { Router } from '@angular/router';

declare var swal:any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  crateFormGroup() {
    return new FormGroup({
      email: new FormControl(''),
      password: new FormControl('')
    });
  }

  loginForm: FormGroup;

  constructor( private _login:LoginService, private _shared: ShareDataService, private router: Router) { 
    this.loginForm = this.crateFormGroup();
  }

  ngOnInit() { 
    if(this._login.getSesion()){
      this.router.navigate(['/']);
    }
    this._shared.setTitle('Inicia Sesion');
  }

  resetForm(){
    this.loginForm.reset();
  }

  saveLogin(){
    this._login.login(this.loginForm.value).subscribe(res =>{
      this._login.setSession(res);
    },
    err =>{
      swal("Error de credenciales","","error",{timer:1000});
    });
  }

}
